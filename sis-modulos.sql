/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE IF NOT EXISTS `modulos` (
  `idModulo` int(11) NOT NULL AUTO_INCREMENT,
  `nomeModulo` varchar(250) NOT NULL,
  `statusModulo` int(10) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idModulo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DELETE FROM `modulos`;
/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
/*!40000 ALTER TABLE `modulos` ENABLE KEYS */;

DROP TABLE IF EXISTS `perfil`;
CREATE TABLE IF NOT EXISTS `perfil` (
  `idPerfil` int(11) NOT NULL AUTO_INCREMENT,
  `nomePerfil` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='Perfils de usuarios criados pelo administrador da corretora';

DELETE FROM `perfil`;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`idPerfil`, `nomePerfil`, `status`) VALUES
	(1, 'Adminstrador', 1),
	(5, 'Usuario', 1);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `idPerfil` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `sobreNome` varchar(50) DEFAULT NULL,
  `email` varchar(120) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `CPF` bigint(11) DEFAULT NULL,
  `senha` varchar(32) NOT NULL,
  `dataNascimento` datetime DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  `dataCadastro` datetime NOT NULL,
  `dataUltimoAcesso` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `detalhes` mediumtext DEFAULT NULL,
  `superAdmin` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'SuperAdmin é o usuario master do sistema.',
  `imgPerfil` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `FK_usuario_perfil` (`idPerfil`),
  CONSTRAINT `FK_usuario_perfil` FOREIGN KEY (`idPerfil`) REFERENCES `perfil` (`idPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Tabela de Usuário do sistema.';

DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
